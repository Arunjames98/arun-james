public class Associate {
       private int associateId=0;
       private String associateName="" , workStatus="";
  
       public int getAssociateId() {
		return associateId;
	}

	public void setAssociateId(int associateId) {
		this.associateId = associateId;
	}

	public String getAssociateName() {
		return associateName;
	}

	public void setAssociateName(String associateName) {
		this.associateName = associateName;
	}

	public String getWorkStatus() {
		return workStatus;
	}

	public void setWorkStatus(String workStatus) {
		this.workStatus = workStatus;
	}

	public void trackAssociateStatus(int day)
       {
              if(day<=20)
              {
                     setWorkStatus("Core skills");
              }
              else if(day<=40)
              {
                     setWorkStatus("Advanced modules");
              }
              else if(day<=60)
              {
                     setWorkStatus("Project phase");
              }
              else
              {
                     setWorkStatus("Deployed in project");
              }
       }
       
       
}
